# Jekyll Pagelink

This Jekyll pluging provides a tag `page_link` that takes a reference to an internal page and generate a html `<a>` beacon.

## Examples

### Automatic href inference


```
{% page_link _howto/gettingstarted.md %}
```


In that case the the plugin will extract from the referenced page `_howto/gettingstarted.md `, the attribute `page.title` in our case `Getting started`
and create the following `<a>` beacon.

```
<a href="/jekyll-theme-newton/howto/gettingstarted/">Getting started</a>
```

### Specifying href text

or you can specify the text of the hyperlink using this syntax

```
{% page_link my super url,_howto/gettingstarted.md %}
```

the result would be

```
<a href="/jekyll-theme-newton/howto/gettingstarted/">my super url</a>
```

### specifiying an anchor

in the page `_pages/example-page.md` we have defined an anchor hparagraph

```
{% page_link _pages/example-page.md#hparagraph %}
```


### specifiying an anchor and href text 

in the page `_pages/example-page.md` we have defined an anchor hparagraph

```
{% page_link here,_pages/example-page.md#hparagraph %}
```





## Installation

Add this line to your Gemfile:

```ruby
group :jekyll_plugins do
  gem "jekyll-pagelink" 
end
```

And then execute:

    $ bundle install

Alternatively install the gem yourself as:

    $ gem install jekyll-pagelink

and put this in your ``_config.yml``

```yaml
plugins:
    - jekyll-pagelink
```

## Release notes


### Version 1.1.1

- consider combining site.url and site.baseurl 

### Version 1.1.0

- Add support of  anchor to the intern site links

### Version 1.0.0

- Initial version, facilitate the creation of intern site links

