module Jekyll
    class PageLink  < Liquid::Tag
        def initialize(tag_name, text, tokens)
            super
            values = text.split(',')
            @hrefText = nil
            @link = nil
            @url = nil

            if values.length == 1
                @url = values[0].strip
            else
                @hrefText = values[0].strip
                @url = values[1].strip
            end

            urlvalues = @url.split('#')
                 
            if urlvalues.length == 1
                @link = urlvalues[0].strip
            else
                @link = urlvalues[0].strip
                @anchor = urlvalues[1].strip
            end
        end

        def render(context)
            site = context.registers[:site]
        
            site.each_site_file do |p|


                if @link == p.relative_path 
                    if @anchor != nil
                        full_url= site.config["url"] + site.config["baseurl"] +  p.url + "/#" + @anchor
                    else
                        full_url= site.config["url"] + site.config["baseurl"] +  p.url
                    end
                    title = @hrefText
                    if @hrefText == nil
                        title = p.data["title"]  
                    end
                    return "<a href=\"#{ full_url }\">#{ title }</a>"
                end
            end
            raise ArgumentError.new <<-eos
                Could not find page "#{@link}" in tag 'page_link'.
                Make sure the post exists and the name and date is correct.
                eos
        end   
    end
end

Liquid::Template.register_tag('page_link', Jekyll::PageLink)